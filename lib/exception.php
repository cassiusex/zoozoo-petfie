<?
class AppException extends Exception {

    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message);
    }

    public function __toString() {
        $text = $this->message;
        $text .= $this->getLine();
        return $text;
    }

    public static function handleError($e) {
        writeToLog($e);
        displayError( $e->getMessage() );
        exit;
    }
}
