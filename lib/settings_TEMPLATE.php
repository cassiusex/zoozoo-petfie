<?php
/** ===============
	App settings
	@Kasian Marszalek, 2013

	!!! DÖP OM TILL settings.php !!!
	
	Add page to tab: 
	https://www.facebook.com/dialog/pagetab?app_id=1411414342426920&redirect_uri=http://facebook.com
	http://apps.facebook.com/zoozoo_petfie
================ */

// include DB
require_once('database.php'); 
require_once('exception.php'); 

class Settings {
	public static $_environment = 'dev';
	public static $_version = '1.0';
	
	public static $_databaseHost = 'localhost';
	public static $_databaseUser = '';
	public static $_databasePassword = '';
	public static $_databaseName = '';
	public static $_databaseCharset = 'utf8';
	
	public static $_applications = array(
		'1411414342426920' => array(
			'app_locale' => 'sv_SE',
			'app_id' => '1411414342426920',
			'app_secret' => '07cb6036ce06e17b8b02810346eb69e3',
			'app_namespace' => 'zoozoo_petfie',
			'app_name' => 'ZooZoo Petfie',
			'page_id' => '294723457211273'
		)
	);	
}
