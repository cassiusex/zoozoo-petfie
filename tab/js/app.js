/**
 * [populate description]
 * @return {[type]} [description]
 */
(function($, app) {
	'use strict';
	
	app.main = {
		scope: ["user_likes", "email"],
		connected: false, 
		container: '#main-content',
		preloader: '<div id="content-preloader"></div>',
		current: null,
		top_friends: null,
		init: function() {
		},	
		countdownView: function() {
			$.ajax({
				dataType: 'json',
				data: {
					action: 'countdown',
				},
				success: function(response)	{
					if(response.status == '200') {
						$(app.main.container).html(response.html); 
					} else {
						console.log(response.status); 
					}
				}
			});			
		},
		switchView: function(which) {
			if(which === app.current) 
				return; 
			
			app.current = which; 

			/** =============================================================== 
				RUN APP
			=============================================================== **/
			$.ajax({
				dataType: 'json',
				data: {
					action: which,
				},
				success: function(response)	{
					if(response.status == '200') {
						$(app.main.container).html(response.html); 					
					} else {
						alert(response.html + ": " + response.status); 
					}
				}
			});
		},
		login: function() {
			var dfd = $.Deferred();
			if ("FB" in window) {
				FB.login(function(response) {
					if (response.authResponse) {
					    window.app.accessToken = response.authResponse.accessToken,
					    window.app.signedRequest = response.authResponse.signedRequest,
					    window.app.userId = response.authResponse.userID;

						dfd.resolve(response);
					} else {
						dfd.reject(response);
					}
				}, {
					scope: app.main.scope.join(',')
				});
			} else {
				dfd.reject();
			}
			return dfd.promise();
		},		
		likesPage: function (pageId) {
			var dfd = $.Deferred();
			
			if ("FB" in window && pageId) {
				FB.api('/me/likes/' + pageId, function (response) {
					if (response.data.length === 0) {
						dfd.reject();
					} else {
						dfd.resolve();
					}
				});
			} else {
				dfd.reject();
			}
			
			return dfd.promise();
		},		
		checkPermissions: function() {
	    	var dfd = $.Deferred();
	    	
	    	if ("FB" in window) {
	    		FB.api('/me/permissions', function(response) {
	    			if (!response.error) {
	    				var permsArray = response.data[0], permsToPrompt = [];
	    				for (var i in app.main.scope_arr) {
	    					if (permsArray[app.main.scope[i]] == null) {
	    						permsToPrompt.push(app.main.scope[i]);
	    					}
	    				}
	    				if (permsToPrompt.length > 0) {
	    					dfd.reject(permsToPrompt);
	    				} else {
	    					dfd.resolve();
	    				}
	    			} else {
	    				dfd.reject();
	    			}
	    		});
	    	} else {
	    		dfd.reject();
	    	}
	    	
	    	return dfd.promise();
	    },		
	    promtForPerms: function(perms) {
	    	var dfd = $.Deferred();
	    	if ("FB" in window && $.isArray(perms)) {
	    		FB.login(function(response) {
	    			dfd.resolve();
	    		}, {
	    			scope: perms.join(',')
	    		});
	    	} else {
	    		dfd.reject();
	    	}
	    	return dfd.promise();
	    },		
	}; 
	
}) (jQuery, window.app);



/* ==========================================================================
   MIDDAGSTESTET
   ========================================================================== */
var SpaceInvaders = function(el) {
	this.$el = $(el);
	
	this.$form = this.$el.find('form');
	this.$status = this.$el.find('.js-status');
	this.$canvas = this.$el.find('.js-processing');
	this.$disclaimer = this.$el.find('.disclaimer'); 
	this.$likeGate = $('#like-gate');

	this.$connected = false; 
	this.$current = 'step-start'; 
	this.$next = ''; 

	this.steps = {
		$start: this.$el.find('.step-start'),
		$upload_1: this.$el.find('.step-upload-1'), 
		$upload_2: this.$el.find('.step-upload-2'), 
		$upload_3: this.$el.find('.step-upload-3'), 
		$gallery: this.$el.find('.step-gallery'), 
		$single: this.$el.find('.step-single')
	};
	
	this.submitUrl = this.$form.attr('data-process-url');
	this.loginUrl = this.$form.attr('data-login-url');
	this.accessToken = 0;
	this.steps.$start.fadeIn(200);
	$("#file-upload-zone").hide(); 
	$("#instagram-area").hide(); 
	$(".code-preloader").hide(); 

	this.$selected_insta = {
		image_medium: '', 
		image_thumb: '', 
		object: null
	}; 	

	this.$current_single = null; 
	this.$current_filter = 'clear'; 
	this.$base64_medium = ''; 
	this.$entry_list = null; 
	this.$current_index = 0; 

	var _this = this;

	$('.link-btn').on('click', function(evt) {
		_this.$next = $(this).data('href'); 
		_this.start.call(_this, evt, this);
	});

	if(window.app.directLink != 0) {
		_this.$next = 'step-single';
		_this.start.call(_this, null, null);	

		if(window.app.directLink != 0) {
		    $.ajax({
		        url: 'ajax/brain.php',
		        dataType: 'json',
		        method: 'post',
		        data: {
			        action: 'get_entry',
					user_id: window.app.directLink, 
					origin_id: window.app.appId
				},
				success: function(response) {
					if(response.error == "no_result") {
						return; 
					}

					_this.changePage('step-single'); 
					_this.buildSingleGallery(response); 
				}
		    });						
		}		
	}	
};

SpaceInvaders.prototype.start = function(evt, el) {
	var _this = this;
	
	if (evt) {
		evt.preventDefault();
	}
	
	this.setStatus('Ansluter med Facebook...', true);

	// already connected
	if(_this.$connected) {
		_this.changePage(_this.$next);		
	} else {
		window.app.main.login().then(function(response) {
			_this.accessToken = response.authResponse.accessToken || '';
			return window.app.main.checkPermissions();
		}, function() {
			_this.setStatus('Kunde inte ansluta med Facebook.');
		}).then(function() {
			window.app.main.likesPage(window.app.pageId).then(function() {
				_this.$connected = true; 
				_this.changePage(_this.$next);
				_this.initUpload(); 
			}, function() {
				_this.setStatus(false);
				_this.likeGate();

				if ("FB" in window) {
					FB.Canvas.setSize({
						height: 400
					});		

					FB.Canvas.setAutoGrow(true); 
					FB.Canvas.scrollTo(0,0);	
				}			
			});
		}, function(missingPerms) {
			if (!missingPerms) {
				return false;
			}
			_this.setStatus('Några rättigheter saknas. <a href="#" id="prompt-for-missing-perms">Godkänn.</a>', false);
			$('#prompt-for-missing-perms').click(function() {
				window.app.main.promtForPerms(app.main.scope).then(function() {
					return window.app.main.checkPermissions();
				}).then(function() {
					FB.Canvas.scrollTo(0,0);
					_this.setStatus(false);
					_this.$connected = true; 
					_this.changePage(_this.$next);
					_this.initUpload(); 
				}, function() {
					// Missing correct perms
				});
			});
		});
	}
};

SpaceInvaders.prototype.setStatus = function(text, spin) {
	if (!text) text = '';

	this.$status.delay(1400).fadeOut(1000, function() {
		$(this).text('').show();
	});
	
	this.$status.html(text);
};

SpaceInvaders.prototype.changePage = function(next) {
	var _this = this;
	this.setStatus('Ansluten.');

	if(_this.$current == next) { return; }

    $.ajax({
        url: 'ajax/brain.php',
        dataType: 'json',
        method: 'post',
        data: {
	        action: 'get_entry',
			user_id: window.app.userId, 
		},
		success: function(response) {
			console.log('change page - check entry', response); 
			var entry = response.entry; 

			if(entry != null && 
				entry.user_id != null && 
				entry.user_id.length > 0 && 
				entry.pet_description != null && 
				entry.pet_description.length > 0 && 
				entry.pet_name != null && 
				entry.pet_name.length > 0 &&
				next == 'step-upload-1') {
				
				_this.changePage('step-single'); 
				_this.buildSingleGallery(response); 
			} else {
				$('.' + _this.$current).fadeOut(100, function() {
					if(entry != null && next == 'step-single') {
						_this.buildSingleGallery(response);
					}

					if(next == 'step-gallery') {
						_this.getEntryListTop($("#entry-list-gallery-top")); 
						_this.getEntryList($("#entry-list-gallery"), 0, 0); 
					}					

					$('.' + next).fadeIn(200); 
					_this.$current = next; 

					if ("FB" in window) {
						FB.Canvas.setSize({
							height: 400
						});		

						FB.Canvas.setAutoGrow(true); 
						FB.Canvas.scrollTo(0,0);	

						$(".back-to-index").unbind('click'); 
						$(".back-to-index").on('click', function() {
							_this.changePage('step-start'); 
						})
					}			
				});					
			}
		}
	}); 		
}; 

/** =============================================================== 
	DESKTOP UPLOAD
=============================================================== **/
SpaceInvaders.prototype.initUpload = function() {
	var _this = this;

	FB.api('/me', function(fbresponse) {
		_this.buildSecondUpload();

		$('#fileupload').fileupload({
	        url: 'ajax/brain.php',
	        dataType: 'json',
	        multipart: true,
	        singleFileUploads: true,
	        autoUpload: true,
	       	previewMaxWidth: 100,
	        previewMaxHeight: 100,
	        previewCrop: true,
	        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
	        maxFileSize: 5000000,
	        formData: {
		        action: 'uploadImage',
		        origin_id: window.app.appId,
				user_id: fbresponse.id, 
		        user_name: fbresponse.name,
			},
	        done: function (e, data) {
		        if(data.textStatus === "success") {
		        	console.log(data.result); 
		        	$("#uploaded-image").html('<img src="' + data.result.image_medium + '">'); 
		        	$("#file-upload-zone").fadeOut(100);

	        		Caman("#uploaded-image img", function() {
						_this.$base64_medium = this.toBase64();
					}); 


		        	if(!window.app.instaphoto) {
		        		window.app.instaphoto = {}; 
		        	}

		        	window.app.instaphoto.image_medium = data.result.image_medium; 
		        	window.app.instaphoto.image_thumb = data.result.image_thumb; 


		        	//_this.buildSecondUpload(); 
		        	//_this.changePage('step-upload-2'); 
		        }
	        },
	    }).on('fileuploadprogressall', function (e, data) {
	        var progress = parseInt(data.loaded / data.total * 100, 10);
	        $('#progress .progress-bar').css(
	            'width',
	            progress + '%'
	        );
    	}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled'); 


	    // btns
    	$("#init-upload").on('click', function() {
    		$("#file-upload-zone").fadeIn(200); 
    		$("#instagram-area").hide(); 
    	}); 

    	$("#init-insta").on('click', function() {
			$("#instagram-area").fadeIn(200); 
    		$("#file-upload-zone").hide(); 
    	}); 

    	$(".cancel-upload, #insta-search-cancel").on('click', function() {
    		$("#file-upload-zone").fadeOut(100); 
    		$("#instagram-area").fadeOut(100); 
    	}); 					
	});


	$("#init-insta").on('click', function() {
		$("#insta-search-submit").on('click', function() {
			$(".insta-search-results").fadeIn(50); 
			$(".code-preloader").fadeIn(50); 
			$(".insta-before-search").fadeOut(50); 

		    $.ajax({
		        url: 'ajax/brain.php',
		        dataType: 'json',
		        method: 'post',
		        data: {
			        action: 'getInstagramGallery',
					user_id: window.app.userId, 
					insta_username: $("#insta-search").val()
				},
				success: function(response) {
					if(response.meta != null && response.meta.code == 200) {
						$(".code-preloader").fadeOut(50); 
						$(".arrow-left, .arrow-right").fadeIn(50); 

						_this.buildInstaPicker(response); 	
					} else if(response === null) {
						alert("Användarnamnet hittades inte.")
						$(".insta-before-search").fadeIn(50); 
						$(".insta-search-results").fadeOut(50); 
					} else {
						alert("Profilen har inga bilder uppladdade eller är inställd på privat."); 
						$(".insta-before-search").fadeIn(50); 
						$(".insta-search-results").fadeOut(50); 
					}
				}
		    });   
		}); 
	}); 


	$("#upload-accept-btn").on('click', function() {
		FB.api('/me', function(fbresponse) {
			if(window.app.instaphoto === null || _this.$base64_medium === '') { return; }
		    $.ajax({
		        url: 'ajax/brain.php',
		        dataType: 'json',
		        method: 'post',
		        data: {
			        action: 'uploadFacebookImage',
					user_id: window.app.userId, 
					image_large: _this.$base64_medium,
					filter: _this.$current_filter,
					user_name: fbresponse.name
				},
				success: function(response) {
			        if(response.status === "success") {
			        	_this.buildSecondUpload(); 
				        _this.changePage('step-upload-2');  
			        }
				}
		    });     		
    	}); 
	}); 




	Caman("#style-2", function () {
		this.lomo();
		this.render(); 
	});	
	Caman("#style-3", function () {
		this.love();
		this.render(); 
	});	
	Caman("#style-4", function () {
		this.herMajesty();
		this.render(); 
	});	
	Caman("#style-5", function () {
		this.oldBoot();
		this.render(); 
	});	
	Caman("#style-6", function () {
		this.glowingSun();
		this.render(); 
	});	
	Caman("#style-7", function () {
		this.hemingway();
		this.render(); 
	});		

	$(".filter-effects li a").on('click', function() {
		var filter = $(this).data('style'); 
		_this.$current_filter = filter; 		

		if(filter == 'clear') {
			Caman("#uploaded-image img, #uploaded-image canvas", function () {
				this.revert(); 
				this.render(function() {
					_this.$base64_medium = this.toBase64();
				}); 
			});	
		} else {
			Caman("#uploaded-image img, #uploaded-image canvas", function () {
				this.revert(); 
				this[filter]();
				this.render(); 

				this.render(function() {
					_this.$base64_medium = this.toBase64();
				}); 
			});	
		}		
	}); 				
}; 

SpaceInvaders.prototype.buildSingleGallery = function(response) {
	var _this = this; 

	$("#search-petfie-submit-1").unbind('click'); 
	$("#search-petfie-submit-1").on('click', function() {
		var search_term = $("#search-petfie-1").val(); 
		$("#search-petfie-1").val(''); 

		if(_this.$current == 'step-single') {
			_this.getEntryList($("#entry-list-single"), 0, 0, search_term); 
		} else {
			_this.getEntryList($("#entry-list-gallery"), 0, 0, search_term); 
		}
	});

	$("#search-petfie-submit-2").unbind('click'); 
	$("#search-petfie-submit-2").on('click', function() {
		var search_term = $("#search-petfie-2").val(); 
		$("#search-petfie-2").val(''); 

		if(_this.$current == 'step-single') {
			_this.getEntryList($("#entry-list-single"), 0, 0, search_term); 
		} else {
			_this.getEntryList($("#entry-list-gallery"), 0, 0, search_term); 
		}
	});	

	if(response.error == "no_result") {
		return; 
	}

	var entry = response.entry; 
	$("#delete-btn").empty(); 

	_this.$current_single = entry.user_id; 
	$("#single-image").html('<img src="' + entry.medium_url + '">'); 
	$("#when-uploaded").html(entry.insert_dt); 
	$("#my-profile-img").attr('src', 'https://graph.facebook.com/' + entry.user_id + '/picture/?type=square'); 
	$("#my-name").attr('href', 'https://facebook.com/' + entry.user_id).html("<strong>" + entry.user_name + "</strong>"); 
	$("#pet-name").html(entry.pet_name); 
	$("#pet-description").html(entry.pet_description); 
	$("#num-likes").html(response.likes); 
	$("#entry-name-owner").html("DELA " + entry.user_name + "S PETFIE!"); 

	$(".share-petfie").unbind('click'); 
	$(".share-petfie").on('click', function(e) {
		e.preventDefault(); 

		FB.ui({
			method: 'feed',
			name: name,
			description: "Rösta på " + entry.user_name + "s petfie!",
			link: 'https://appclient.net/custom_htdocs/zoozoo/tab/share.php?origin_id=1411414342426920&id=' + entry.user_id,
			picture: "http://appclient.net/custom_htdocs/zoozoo/tab/img/400-share.png"
		}, function(response) {
			if (response && !response.error_code) {
				$("#code-popup").fadeIn(200); 
			}
		});
	}); 	 

	var comment_container = $("#entry-comment-box"); 
	comment_container.empty(); 
	$.each(response.comments, function(index, value) {
		var comment = $('<div />')
		.addClass('comment'); 

		var profile = $('<div />')
		.addClass('profile profile-comment clearfix'); 

		var img = $('<img />')
		.addClass('pull-left')
		.attr('id', 'canan-img-single')
		.attr('src', 'https://graph.facebook.com/' + value.user_id + '/picture/?type=square'); 		

		var name = $('<a />')
		.attr('href', 'https://facebook.com/' + value.user_id)
		.attr('target', '_blank')
		.html(value.user_name); 

		profile.append(img)
		.append(name)
		.append('&nbsp;')
		.append(value.comment_text); 

		comment.append(profile); 
		comment_container.append(comment); 
	}); 




	$(".arrow-next, .next").unbind('click'); 
	$(".arrow-next, .next").on('click', function() {
		_this.$current_index = _this.$current_index >= _this.$entry_list.length - 1 ? _this.$current_index : _this.$current_index + 1; 
		_this.buildSingleGallery(_this.$entry_list[_this.$current_index]); 
	}); 


	$(".arrow-prev, .prev").unbind('click'); 
	$(".arrow-prev, .prev").on('click', function() {
		_this.$current_index = _this.$current_index <= 0 ? _this.$current_index : _this.$current_index - 1; 
		_this.buildSingleGallery(_this.$entry_list[_this.$current_index]); 
	}); 



	// Build entry list
	_this.getEntryList($("#entry-list-single"), 0, 0); 

	FB.api('/me', function(fbresponse) {
		$("#submit-comment").unbind('click'); 
		$("#submit-comment").on('click', function() {
			_this.addComment(entry.user_id, fbresponse.id, fbresponse.name, $("#comment-field")); 
		}); 

		$("#comment-field").unbind('keyup'); 
		$('#comment-field').on('keyup', function(e) {
			if(e.keyCode == 13) {
				_this.addComment(entry.user_id, fbresponse.id, fbresponse.name, $(this)); 
			}
		});		

		$("#like-string").unbind('click'); 
		$("#like-string").on('click', function() {
			_this.like(entry.user_id, fbresponse.id); 
		}); 		

		if(entry.user_id == fbresponse.id) {
			var del_btn = $('<a />').addClass('pull-right text-danger text-small')
									.attr('id', 'delete-petfie')
									.attr('href', '#')
									.html('<span class="glyphicon glyphicon-warning-sign"></span> Radera ditt bidrag'); 
			$("#delete-btn").html(del_btn); 

			// Delete entry
			del_btn.unbind('click'); 
			del_btn.on('click', function(e) {
				e.preventDefault(); 

				var yes = confirm('Är du säker på att du vill ta bort ditt bidrag? Alla kommentarer och gilla-markeringar tas bort.'); 
				if(yes) {
					$.ajax({
				        url: 'ajax/brain.php',
				        dataType: 'json',
				        method: 'post',
				        data: {
					        action: 'delete_entry',
					        user_id: window.app.userId
						},
						success: function(response) {
							if(response.status == 'deleted') {
								_this.changePage('step-start'); 
							} else {
								alert("Någonting gick fel, försök gärna igen om några sekunder"); 
							}
						}
					}); 
				}
			}); 			
		}		
	}); 	
}; 

// prev = -1, next = 1
SpaceInvaders.prototype.nextSingle = function(prevNext) {
}; 

SpaceInvaders.prototype.addComment = function(entry_id, user_id, user_name, comment_field) {
	comment_text = comment_field.val(); 

	if(comment_text.length > 1) {
		$("#submit-btn").addClass('disabled'); 

	    $.ajax({
	        url: 'ajax/brain.php',
	        dataType: 'json',
	        method: 'post',
	        data: {
		        action: 'post_comment',
		        entry_id: entry_id,
				user_id: user_id,
		        user_name: user_name,
		        comment_text: comment_text
			},
			success: function(response) {
				if(response.status == 'success') {
					$("#submit-btn").removeClass('disabled'); 
					comment_field.val('');
					comment_field.empty(); 

					var comment = $('<div />')
					.addClass('comment'); 

					var profile = $('<div />')
					.addClass('profile profile-comment clearfix'); 

					var img = $('<img />')
					.addClass('pull-left')
					.attr('src', 'https://graph.facebook.com/' + user_id + '/picture/?type=square'); 

					var name = $('<a />')
					.attr('href', 'https://facebook.com/' + user_id)
					.attr('target', '_blank')
					.html(user_name); 

					profile.append(img)
					.append(name)
					.append('&nbsp;')
					.append(comment_text); 

					comment.append(profile); 

					$("#entry-comment-box").prepend(comment); 
				}
			}
	    });
	}
}; 

SpaceInvaders.prototype.like = function(entry_id, user_id) {
	var _this = this; 

	$.ajax({
        url: 'ajax/brain.php',
        dataType: 'json',
        method: 'post',
        data: {
	        action: 'like',
	        entry_id: entry_id,
			user_id: user_id,
		},
		success: function(response) {
			if(response.status == 'success') {
			    $.ajax({
			        url: 'ajax/brain.php',
			        dataType: 'json',
			        method: 'post',
			        data: {
				        action: 'get_entry',
						user_id: entry_id 
					},
					success: function(response) {
						_this.buildSingleGallery(response); 
					}
			    });				
			}
		}
    });
}; 


SpaceInvaders.prototype.getEntryListTop = function(container) {
	var _this = this; 

    $.ajax({
        url: 'ajax/brain.php',
        dataType: 'json',
        method: 'post',
        data: {
	        action: 'get_entries_top',
		},
		success: function(response) {
			container.empty(); 
			$.each(response, function(index, entry) {
				var full_entry = $('<div />')
				.addClass('entry list-entry')
				.append(_this.buildListEntry(entry, 'top', index + 1)); 

				full_entry.find('img').on('click', function() {
					if(_this.$current != 'step-single') {
						$('.' + _this.$current).fadeOut(100, function() {
							$('.' + 'step-single').fadeIn(200); 
							_this.$current = 'step-single'; 

							if ("FB" in window) {
								FB.Canvas.setSize({
									height: 400
								});		

								FB.Canvas.setAutoGrow(true); 
								FB.Canvas.scrollTo(0,0);	
							}			
						});	
					}

					_this.buildSingleGallery(entry); 
				}); 

				container.append(full_entry); 
			}); 
		}
    }); 
}; 

// sorting: 'latest', 'votes'; pagination: {1, 16}
SpaceInvaders.prototype.getEntryList = function(container, sorting, pagination, search_term) {
	var _this = this; 

    $.ajax({
        url: 'ajax/brain.php',
        dataType: 'json',
        method: 'post',
        data: {
	        action: 'get_entries',
			sorting: sorting,
			pagination: pagination, 
			search_term: search_term
		},
		success: function(response) {
			container.empty(); 

			_this.$entry_list = response; 

			$(".total-entries").html(response.length); 

			$.each(response, function(index, entry) {
				var full_entry = $('<div />')
				.addClass('entry list-entry')
				.append(_this.buildListEntry(entry)); 

				full_entry.find('img').on('click', function() {
					if(_this.$current != 'step-single') {
						$('.' + _this.$current).fadeOut(100, function() {
							$('.' + 'step-single').fadeIn(200); 
							_this.$current = 'step-single'; 

							if ("FB" in window) {
								FB.Canvas.setSize({
									height: 400
								});		

								FB.Canvas.setAutoGrow(true); 
								FB.Canvas.scrollTo(0,0);	
							}			
						});	
					}

					_this.buildSingleGallery(entry); 
					_this.$current_index = _this.$entry_list.indexOf(entry); 
				}); 

				container.append(full_entry); 
			}); 
		}
    }); 
}; 


SpaceInvaders.prototype.buildListEntry = function (entry, top, pos) {
	var markup = '<div class="profile">' + 
			'<img class="pull-left caman-img" src="https://graph.facebook.com/' + entry.entry.user_id + '/picture/?type=square">' + 
			'<a href="https://facebook.com/' + entry.entry.user_id + '" target="_blank">' + entry.entry.user_name + '</a><br>' + 
			'<span class="text-muted">' + entry.entry.insert_dt + '</span>' + 
		'</div>' + 
		'<img src="' + entry.entry.medium_url + '">' + 
		'<div class="like-field left">' + 
			'<p class="like-string"><span class="glyphicon glyphicon-heart"></span>&nbsp;<strong>' + entry.likes + '</strong> Gilla-markeringar</p>' + 
			'<p><img src="img/bubble.png"> <strong>' + entry.entry.pet_name + '</strong> <span>' + entry.entry.pet_description + '</span></p>' + 
		'</div>'; 

	if(top == 'top') {
		markup2 = '<div class="profile">' + 
			'<div class="ribbon-wrapper"><div class="ribbon ribbon-' + pos + '">' + pos + '#</div></div>' + 
			'<img class="pull-left caman-img" src="https://graph.facebook.com/' + entry.entry.user_id + '/picture/?type=square">' + 
			'<a href="https://facebook.com/' + entry.entry.user_id + '" target="_blank">' + entry.entry.user_name + '</a><br>' + 
			'<span class="text-muted">' + entry.entry.insert_dt + '</span>' + 
		'</div>' + 
		'<img src="' + entry.entry.medium_url + '">' + 
		'<div class="like-field left">' + 
			'<p class="like-string"><span class="glyphicon glyphicon-heart"></span>&nbsp;<strong>' + entry.likes + '</strong> Gilla-markeringar</p>' + 
			'<p><img src="img/bubble.png"> <strong>' + entry.entry.pet_name + '</strong> <span>' + entry.entry.pet_description + '</span></p>' + 
		'</div>'; 

		return markup2; 
	}

	return markup; 
}; 

SpaceInvaders.prototype.buildSecondUpload = function() {
	var _this = this; 

	FB.api('/me', function(fbresponse) {
	    $.ajax({
	        url: 'ajax/brain.php',
	        dataType: 'json',
	        method: 'post',
	        data: {
		        action: 'get_entry',
				user_id: fbresponse.id, 
			},
			success: function(response) {
				console.log(" OHJKJFLKFJ ", response); 
				if(response.entry.user_id) {
			        $("#uploaded-image-second").html('<img src="' + response.entry.medium_url + '">');        

			        $("#age_met").on('change', function() {
			        	if($(this).is(':checked')) {
							$("#submit-form-btn").removeAttr('disabled'); 
			        	} else {
							$("#submit-form-btn").attr('disabled', 'disabled'); 
						}
			        }); 


			        $("#back-upload").on('click', function() {
			        	_this.changePage('step-upload-1'); 
			        	_this.initUpload(); 
			        }); 		
		        }        
			}
	    }); 
    }); 	
}; 

SpaceInvaders.prototype.buildThirdUpload = function() {
	var _this = this; 

    $.ajax({
        url: 'ajax/brain.php',
        dataType: 'json',
        method: 'post',
        data: {
	        action: 'get_entry',
			user_id: window.app.userId, 
		},
		success: function(response) {
	        $("#short-link").attr('href', response.short_url).html(response.short_url); 
			var full_entry = $('<div />')
			.addClass('entry list-entry')
			.append(_this.buildListEntry(response)); 	  


			console.log("***** HEJ ****", response); 
			$("#share-twitter").attr('href', 'http://twitter.com/share?url=' + encodeURI('https://appclient.net/custom_htdocs/zoozoo/tab/share.php?origin_id=1411414342426920&id=' + window.app.userId));       
			$("#share-pinterest").attr('href', 'https://www.pinterest.com/pin/create/button/?url=https://appclient.net/custom_htdocs/zoozoo/tab/share.php?origin_id=1411414342426920&id=' + window.app.userId + '&media=' + response.entry.medium_url + "&description=" + encodeURIComponent('Rösta på min petfie!')); 
			$("#share-google").attr('href', 'https://plus.google.com/share?url=https://appclient.net/custom_htdocs/zoozoo/tab/share.php?origin_id=1411414342426920&id=' + window.app.userId); 

			$("#share-facebook").on('click', function(e) {
				e.preventDefault(); 

				FB.ui({
					method: 'feed',
					name: name,
					description: "Rösta på min petfie!",
					link: 'https://appclient.net/custom_htdocs/zoozoo/tab/share.php?origin_id=1411414342426920&id=' + window.app.userId,
					picture: "http://appclient.net/custom_htdocs/zoozoo/tab/img/400-share.png"
				}, function(response) {
					if (response && !response.error_code) {
						$("#code-popup").fadeIn(200); 
					}
				});
			}); 

	        $("#petfie-entry").append(full_entry); 
		}
    }); 	
}; 

SpaceInvaders.prototype.buildInstaPicker = function(data) {
	var _this = this;

	if(data.length <= 0) {
		return; 
	}

	var container = $("#insta-results"); 
	container.empty(); 

	$.each(data.data, function(index, value) {
		var image_medium = value.images.standard_resolution.url; 
		var image_thumb = value.images.thumbnail.url; 

		var gallery_object = $("<div />")
		.addClass("instapicker-image")
		.append('<img src="' + image_thumb + '">'); 

		gallery_object.on('click', function() {
			$.ajax({
		        url: 'ajax/brain.php',
		        dataType: 'json',
		        method: 'post',
		        data: {
			        action: 'save_insta',
					user_id: window.app.userId, 
					image_medium: image_medium,
					image_thumb: image_thumb
				},				
				success: function(response) {
					if(response.status == 'ok') {
						if(_this.$selected_insta.object != null) {
							_this.$selected_insta.object.removeClass('marked'); 
						}

						_this.$selected_insta = {
							image_medium: response.image_medium, 
							image_thumb: response.image_thumb, 
						}; 

						window.app.instaphoto = _this.$selected_insta; 
						$("#uploaded-image").empty().delay(50).html('<img src="' + response.image_medium + '">');  

						Caman("#uploaded-image img", function() {
							_this.$base64_medium = this.toBase64();
						}); 

						$(this).addClass('marked'); 						
					}
				}
			})
		}); 

		container.append(gallery_object); 
	});

	$('#insta-results .instapicker-image:first').before($('#insta-results .instapicker-image:last'));

    $('#arrow-right').on('click.right', function() {
        var item_width = $('#insta-results .instapicker-image').outerWidth() + 12;
        var left_indent = parseInt($('#insta-results').css('left')) - item_width;

        $('#insta-results').animate({'left' : left_indent},{queue:false, duration:500},function() {
            $('#insta-results .instapicker-image:last').after($('#insta-results .instapicker-image:first'));
        	$('#insta-results').css({'left' : '-72px'});
        });

    });

    $('#arrow-left').on('click.left', function() {
        var item_width = $('#insta-results .instapicker-image').outerWidth() + 12;
        var left_indent = parseInt($('#insta-results').css('left')) + item_width;

        $('#insta-results').animate({'left' : left_indent},{queue:false, duration:500},function() {
	        $('#insta-results .instapicker-image:first').after($('#insta-results .instapicker-image:last'));
        	$('#insta-results').css({'left' : '-72px'});
        });
    });			


}; 

// btn_share.on('click', function(e) {
// 	e.preventDefault(); 
// 	if ("FB" in window) {
// 		FB.ui({
// 			method: 'feed',
// 			name: name,
// 			description: description,
// 			link: link,
// 			picture: "http://appclient.net/custom_htdocs/tmnt_test/tab/img/400-share.png"
// 		}, function(response) {

// 		});
// 	}
// }); 

SpaceInvaders.prototype.likeGate = function() {
	var _this = this; 
	
	if ("FB" in window) {
		$('body').addClass('dontLike');
		FB.Canvas.scrollTo(0, 0);
		FB.Canvas.setSize({height: 860});
		FB.XFBML.parse();

		FB.Event.subscribe('edge.create', function(response) {
			var url = response.replace(/^\s+|\s+$/g, '');
			if (url == ('https://www.facebook.com/' + window.app.pageId) || url == ('http://www.facebook.com/' + window.app.pageId)) {
				$('body').removeClass('dontLike');
				_this.start.call(_this);
			}
		});
	}
};

$(window).load(function() {
	if ("FB" in window) {
		FB.Canvas.setAutoGrow(false);
		FB.Canvas.setSize({
			height: 860
		});
	}
});

var SpaceInvaders = new SpaceInvaders('#spaceinvaders');	

$.validator.setDefaults({
	submitHandler: function() {
		FB.api('/me', function(response) {
			var data = { 
				action: 'signup', 
				user_id:  response.id, 
				user_name: response.name
			};

			$.each($('form input[type="text"],form input[type="email"], form input[type="checkbox"]:checked, form input[type="hidden"], form textarea'), function() {
				console.log($(this).attr('name'), $(this).val()); 

				if($(this).attr('name')) {
					data[$(this).attr('name')] = $(this).val();
				}
			});

			if(!$("#age_met").is(':checked')) {
				console.log("Age not met."); 
				return; 
			}			

			console.log("data", data); 

			$.ajax({
				data: data,
				method: 'post',
				dataType: 'json',
				success: function(response) {
					if(response.status == 'success') {
						$("#petfie-form").fadeOut(300, function() {
							SpaceInvaders.buildThirdUpload(); 
							SpaceInvaders.changePage('step-upload-3'); 
						}); 
					} else {
						alert("Någonting gick fel, försok igen om några minuter."); 
					}
				}
			});
		}); 
	}
});	


$("#petfie-form").validate(); 