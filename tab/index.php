<?php 
require_once('../lib/settings.php');
require_once('../inc/lib/facebook.php'); 


/* ============================================================================
	APP INIT
============================================================================= */
$db = new Database(); 
$origin_id = $db->escape($_GET['origin_id']); 


/* ============================================================================
	MOBILE CHECK
============================================================================= */
$mobile = false;
if(isset($_GET["mobile"]) && $_GET["mobile"] == 1) {
	$mobile = true;  
}


// https://appclient.net/custom_htdocs/zoozoo/tab/share.php?origin_id=1411414342426920&id={ID}
if(isset($_COOKIE["ftm_petfie_id"]) && $_COOKIE["ftm_petfie_id"] != null) {
	$direct_link = $db->escape($_COOKIE["ftm_petfie_id"]);
	//echo $direct_link; 
}

/* ============================================================================
	HTML INIT
============================================================================= */
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo Settings::$_applications[$origin_id]['app_name']; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="cache-control" content="no-cache">
	<meta property="og:title" content="ZooZoo Petfie!"/>
	<meta property="og:url" content="http://apps.facebook.com/zoozoo_petfie"/>
	<meta property="og:image" content="http://appclient.net/custom_htdocs/zoozoo_petfie/tab/img/400-share.png"/>
	<meta property="og:description" content="ZooZoo Petfie!" />

	<!-- css includes -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animate.css">	
	<link rel="stylesheet" href="css/style.css">	
</head>
<body>

	<!-- main content -->
	<div id="spaceinvaders">

		<div id="like-gate" class="like-gate">
			<h1>Gilla sidan för att vara med att<br>
			tävla eller rösta!</h1>
			<img class="main-logo" src="img/like-arrow.png" style="position: absolute; top: -30px; left: 250px;">

			<div class="like-gate-btn">
				<div class="fb-like" data-href="https://www.facebook.com/<?php echo Settings::$_applications[$origin_id]['page_id']; ?>" data-send="false" data-layout="box_count" data-width="60" data-show-faces="false"></div>
			</div>
		</div>		

		<!-- Page: START -->
		<div class="step step-start">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">
						<img src="img/header-pic.png">
						<img src="img/bidrag-rabatt.png" class="floating">
					</div>
					<div class="col-sm-9">
						<img src="img/logo.png"><br>
						<h1>Har ditt husdjur tagit en perfekt Petfie?</h1>
						<h2>Välkommen till årets roligaste tävling för djurägare!</h2>
						<p>Gå vidare i tävlingen nedan, låt dig inspireras på vägen &amp;
						ladda upp ditt husdjurs #ZooZooPetfie. <span class="emph-orange">Vinn en Samsung Galaxy S5 &amp; 10.000 kr!</span>
						</p>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="collage-pic">
							<img src="img/petfie-collage.png">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-8">
						<div class="box-bg box-bg-bulldog">
							<h5>JURY: <strong>BOSS THE FRENCH BULLDOG!</strong></h5>
							<a class="emph-blue" href="#">@bossthefrenchbulldog</a>

							<p>Svensk Petfie-Kung med över 200.000 följare på Instagram.</p>
						</div>
					</div>

					<div class="col-sm-4">
						<button class="btn btn-block btn-orange link-btn" data-href="step-upload-1">TÄVLA!</button>
						<button class="btn btn-block btn-lightblue link-btn" data-href="step-gallery">Rösta här</button>
					</div>
				</div>
			</div>

			<div class="disclaimer">
				<p class="text-muted">All persondata behandlas enligt PUL. Tävlingen är inte administrerad eller sponsrad av Facebook.</p>
			</div>		
		</div>
		<!-- #/Page: START -->


		<!-- Page: UPLOAD 1 -->
		<div class="step step-upload-1">
			<div class="container-fluid">
				<div class="dark-bg">
					<div class="row">
						<div class="center col-sm-12">
							<img class="back-to-index" src="img/logo-white.png">
							<p>&nbsp;</p>
						</div>
					</div>

					<div class="row upload-btns">
						<div class="col-sm-1"></div>

						<div class="col-sm-5">					    			
							<button class="btn btn-block btn-orange btn-upload" id="init-upload"><img src="img/logo-upload.png">&nbsp;LADDA UPP!</button>
						</div>

						<div class="col-sm-5">
							<button class="btn btn-block btn-lightblue btn-upload" id="init-insta"><img src="img/logo-insta.png">&nbsp;INSTAGRAM</button>					
						</div>					

						<div class="col-sm-1"></div>					
					</div>

					<div id="instagram-area">
						<div class="row insta-before-search">
							<div class="col-sm-12 center">
								<input type="text" class="form-control insta-search" id="insta-search" name="insta-search" placeholder="Ditt Instagram-smeknamn">
								<p class="btn btn-lightblue" id="insta-search-submit">Sök</p>
						   		<p class="btn btn-orange" id="insta-search-cancel">Avbryt</p>
							</div>
						</div>

						<div class="row insta-search-results">
							<div class="col-sm-12">
								<div class="code-preloader">
									<div class="code-preloader-spin">
										<span></span>
										<span></span>
										<span></span>
										<span></span>
										<span></span>
									</div>
								</div>

								<div class="arrow-left" id="arrow-left"><img src="img/arrow-left.png"></div>

								<div class="insta-results-wrapper">
									<div id="insta-results"></div>
								</div>

								<div class="arrow-right" id="arrow-right"><img src="img/arrow-right.png"></div>
						    </div>	    
						</div>									
					</div>
			
					<div id="file-upload-zone">
						<div class="row">
							<div class="col-sm-1"></div>
							<div class="col-sm-7">
							    <span class="dropzone fileinput-button">
							        <i class="glyphicon glyphicon-plus"></i>
							        <span>Släpp bilden här eller klicka<br>för att ladda upp din bild</span>
							        <input id="fileupload" multipart="true" type="file" name="files[]">
							    </span>							
							    <div id="progress" class="progress">
							        <div class="progress-bar progress-bar-success"></div>
							    </div>
							    
							</div>
							<div class="col-sm-3">
								<div id="files" class="files"></div>
								<button class="btn btn-orange btn-block cancel-upload">Avbryt</button>
							</div>
							<div class="col-sm-1"></div>
						</div>									
					</div>				

					<div class="row">
						<div class="col-sm-1"></div>

						<div class="col-sm-10 center">
							<div id="uploaded-image">
								<img src="img/placeholder-upload.png">
							</div>
							<ul class="filter-effects">
								<li><a href="#" data-style="clear"><img id="style-1" src="img/placeholder-upload.png"></a></li>
								<li><a href="#" data-style="lomo"><img id="style-2" src="img/placeholder-upload.png"></a></li>
								<li><a href="#" data-style="love"><img id="style-3" src="img/placeholder-upload.png"></a></li>
								<li><a href="#" data-style="herMajesty"><img id="style-4" src="img/placeholder-upload.png"></a></li>
								<li><a href="#" data-style="oldBoot"><img id="style-5" src="img/placeholder-upload.png"></a></li>
								<li><a href="#" data-style="hemingway"><img id="style-6" src="img/placeholder-upload.png"></a></li>
							</ul>							
						</div>	

						<div class="col-sm-1"></div>
					</div>	

					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 center">
							<img src="img/petfie-logo.png" style="width: 240px;">
							<button class="btn btn-green btn-green-accept" style="margin-right: 8px; margin-left: 12px;" id="upload-accept-btn">NÄSTA&nbsp;<img src="img/arrow-right.png"></button>						
						</div>
					</div>							
				</div>
			</div>

			<div class="disclaimer">
				<p class="text-muted">All persondata behandlas enligt PUL. Tävlingen är inte administrerad eller sponsrad av Facebook.</p>
			</div>				
		</div>

		<!-- Page: UPLOAD 2 -->
		<div class="step step-upload-2">
			<div class="container-fluid">
				<div class="dark-bg">
					<div class="row">
						<div class="col-sm-3">
							<div class="btn btn-orange" id="back-upload">Tillbaka</div>
						</div>
						<div class="center col-sm-6">
							<img class="back-to-index" src="img/logo-white.png">
						</div>
						<div class="col-sm-3"></div>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<p>&nbsp;</p>
						</div>
					</div>						

					<div class="row">
						<div class="col-sm-6">
							<div class="uploaded-img" id="uploaded-image-second"></div>

							<img src="img/petfie-logo.png">
						</div>

						<div class="col-sm-6">
							<h1>Grrrrr...</h1>
							<h2>Det ser bra ut!</h2>
							<p>Du är snart klar med din Petfie. Fyll i informationen nedan, 
							godkänn och gå vidare till det sista och viktigaste steget av tävlingen!</p>

							<form id="petfie-form">
								<div id="form-space" class="row">
									<div class="form-group col-sm-12">	
										<label>Skriv in ditt husdjurs namn</label>
										<input type="text" class="form-control" name="pet_name" placeholder="Skriv in ditt husdjurs namn" required>						
									</div>

									<div class="form-group col-sm-12">	
										<label>Ange din e-postadress</label>
										<input type="text" class="form-control" name="user_email" placeholder="Ange din e-postadress" required email>						
									</div>

									<div class="form-group col-sm-12">	
										<label>Ge din Petfie en passande beskrivning</label>
										<textarea rows="6" class="form-control" name="pet_description" placeholder="Ge din Petfie en passande beskrivning" required></textarea>				
									</div>								

						            <div class="form-group col-sm-12">
						                <div class="checkbox">
						                    <label>
						                        <input type="checkbox" name="age_met" id="age_met">
						                        Jag är över 18 år och har läst samt accepterar tävlingsreglerna 
						                    </label>
						                </div>
						                <div class="checkbox">
						                    <label>
						                        <input type="checkbox" name="user_opt_email" id="user_opt_email" checked="checked">
						                        Jag vill få tips &amp; tricks för mitt husdjur samtbra erbjudanden till min epost. 
						                    </label>
						                </div>
						            </div>								

									<div class="form-group col-sm-12">
										<!-- FIXA TILL -->
										<button type="submit" id="submit-form-btn" class="btn btn-green" disabled><img src="img/logo-accept.png">&nbsp;GODKÄNN</button>								
									</div>
								</div>
							</form>
						</div>					
					</div>	

					<div class="row">
						<div class="col-sm-12 disclaimer-upload">
							<p><strong>Regler & Villkor</strong><br>
							Du måste vara minst 18 år för att delta i ZooZoo’s Petfietävling. Du garanterar att du äger rättigheterna till bilden som du deltar med.
							Vid misstanke om fusk, så som att du inte äger rättigheterna till bilden så förbihåller ZooZoo.com sig att radera ditt bidrag samt vidta
							åtgärder om anmälan sker gentemot din bild. Bilden får inte heller innehållande stötande material eller avvika från tävlingens tema.<br>
							<a href="#">Läs mer här.</a></p>
						</div>
					</div>
				</div>						
			</div>

			<div class="disclaimer">
				<p class="text-muted">All persondata behandlas enligt PUL. Tävlingen är inte administrerad eller sponsrad av Facebook.</p>
			</div>						
		</div>		

		<!-- Page: UPLOAD 3 -->
		<div class="step step-upload-3">
			<div class="code-popup" id="code-popup">
				<div class="dim"></div>
				<div class="container-popup">
					<div class="inner">
						<h4>Här kommer din personliga kod:</h4>
						<h1>Xy7940ZZ-50</h1>

						<button class="btn btn-green btn-green-accept link-btn" data-href="step-single">NÄSTA&nbsp;<img src="img/arrow-right.png"></button>
						<p>&nbsp;</p>
						<p>Värde 50 kr. Anges i kassan.<br>
						Giltig vid ett köp t.o.m. 2014.09.01</p>
					</div>
				</div>
			</div>		

			<div class="container-fluid">
				<div class="row">
					<div class="center col-sm-12">
						<img class="back-to-index" src="img/logo.png">
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<p>&nbsp;</p>
					</div>
				</div>						

				<div class="row">
					<div class="col-sm-4">
						<div class="petfie-entry" id="petfie-entry">
						</div>
					</div>

					<div class="col-sm-8">
						<h1>Perfekt!</h1>
						<h2>Nu återstår bara det viktigaste steget...</h2>

						<p>Ingen Petfie är komplett utan likes. Välj vart du vill dela din Petfie utöver
						Facebook och börja samla gilla-markeringar!</p>

						<p>OBS! Få 50 kr att handla för när du delar din Petfie på mer än Facebook.</p>

						<hr>

						<div class="center">
							<p class="emph-blue">Dela på:</p>
							<ul class="share-list">
								<li>
									<a id="share-facebook" href="#">
										<img src="img/logo-facebook.png">
										Facebook
									</a>
								</li>

								<li>
									<a target="_blank" id="share-twitter" href="#">
										<img src="img/icon-twit.png">
										Twitter
									</a>
								</li>

								<li>
									<a target="_blank" id="share-pinterest" href="#">
										<img src="img/icon-pin.png">
										Pinterest
									</a>
								</li>

								<li>
									<a target="_blank" id="share-google" href="#">
										<img src="img/icon-google.png">
										Google+
									</a>
								</li>																								
							</ul>

							<h4 class="emph-blue">Direktlänk till din Petfie: <a id="short-link" target"=_blank" href="#"></a></h4>

							<p>&nbsp;</p>

							<button class="btn btn-green btn-green-accept link-btn" data-href="step-single">NÄSTA&nbsp;<img src="img/arrow-right.png"></button>
						</div>
					</div>					
				</div>	
			</div>

			<div class="disclaimer">
				<p class="text-muted">All persondata behandlas enligt PUL. Tävlingen är inte administrerad eller sponsrad av Facebook.</p>
			</div>			
		</div>		
		<!-- #/Page: UPLOAD -->


		<!-- Page: GALLERY -->
		<div class="step step-gallery">
			<div class="container-fluid">
				<div class="row">
					<div class="center col-sm-12">
						<img class="back-to-index" src="img/logo.png">
					</div>
				</div>

				<!--// Multiple entries -->
				<div class="row">
					<div class="col-sm-12">
						<h2>TOPP 3 - Mest Gillade</h2>
					</div>							
				</div>	

				<div class="row">
					<div class="col-sm-12">
						<hr style="margin-top: -4px; margin-bottom: 6px;">
					</div>
				</div>					

				<div class="row">
					<div class="col-sm-12">
						<div class="entry-list" id="entry-list-gallery-top"></div>
					</div>
				</div>					

				<!--// Multiple entries -->
				<div class="row">
					<div class="col-sm-8">
						<h2>Senast uppladdade Petfies</h2>
					</div>

					<div class="col-sm-4">
						<div class="form-inline right">
							<div class="form-group">
								<input class="form-control" placeholder="Sök efter Petfie" id="search-petfie-1">
							</div>
							<button class="btn btn-success" id="search-petfie-submit-1">Sök</button>
						</div>
					</div>									
				</div>		

				<div class="row">
					<div class="col-sm-12 clearfix">
						<hr style="margin-top: 6px; margin-bottom: 6px;">
						<ul class="view-count">
							<li class="active">
								<a href="#">Visa 15 st</a>
							</li>
							<li>
								<a href="#">Visa 45 st</a>
							</li>
							<li>
								<a href="#">Visa alla</a>
							</li>															
						</ul>

						<h4 class="pull-right" style="margin-top: 0px;">
							Totalt antal <strong class="total-entries">0</strong>
						</h4>
					</div>
				</div>		

				<div class="row">
					<div class="col-sm-12">
						<hr style="margin-top: -4px; margin-bottom: 6px;">
					</div>
				</div>		


				<div class="row">
					<div class="col-sm-12">
						<p>Sortera efter: <button class="btn btn-default">Senast uppladdade</button></p>
					</div>
				</div>			

				<div class="row">
					<div class="col-sm-12">
						<div class="entry-list" id="entry-list-gallery"></div>
					</div>
				</div>												
			</div>

			<div class="disclaimer">
				<p class="text-muted">All persondata behandlas enligt PUL. Tävlingen är inte administrerad eller sponsrad av Facebook.</p>
			</div>			
		</div>
		<!-- #/Page: GALLERY -->

		<!-- Page: SINGLE -->
		<div class="step step-single">
			<div class="container-fluid">
				<div class="row">
					<div class="center col-sm-12">
						<img class="back-to-index" src="img/logo.png">


						<div class="next-prev clearfix">
							<div class="prev">&laquo; Förgående</div>
							<div class="next">Nästa &raquo;</div>
						</div>

						<hr>
					</div>
				</div>

				<!--// Single entry -->
				<div class="row">
					<div class="col-sm-12 center">
						<div class="arrow-prev">
							<span class="glyphicon glyphicon-chevron-left" id="previous-entry"></span>
						</div>

						<!-- SINGLE ENTRY START -->
						<div class="petfie-entry entry-single">
							<div class="row">
								<!-- pic -->
								<div class="col-sm-7 petfie-image" id="single-image">
									<img src="http://placehold.it/400x400">
								</div>

								<!-- comments etc. -->
								<div class="col-sm-5 small-column">
									<div class="profile" id="profile-single">
										<span id="delete-btn" class="pull-right"></span>
										<img class="pull-left" id="my_profile_img" src="">
										<a href="#" target="_blank" id="my-name"></a><br>
										<span class="text-muted" id="when-uploaded"></span>
									</div>

									<hr class="small-hr">

									<div class="like-field left">	
										<p class="like-string" id="like-string"><span class="glyphicon glyphicon-heart"></span>&nbsp;<strong id="num-likes"></strong> Gilla-markeringar</p>
										<p><img src="img/bubble.png"> <strong id="pet-name"></strong> <span id="pet-description"></span></p>
									</div>

									<p class="share-petfie">
										<span class="glyphicon glyphicon-share"></span>
										<span class="uppercase" id="entry-name-owner">
									</p>

									<!-- COMMENTS START -->
									<div class="comment-box" id="entry-comment-box">
									</div>
									<!-- COMMENTS END -->

									<hr class="small-hr">

									<div class="new-comment">
										<div class="form-inline center">
											<div class="form-group">
												<input class="form-control" placeholder="Skriv en kommentar" id="comment-field">
											</div>
											<button class="btn btn-default" id="submit-comment">Skicka</button>
										</div>										
									</div>
								</div>
							</div>
						</div>
						<!-- SINGLE ENTRY END --> 

						<div class="arrow-next">
							<span class="glyphicon glyphicon-chevron-right" id="next-entry"></span>
						</div>
					</div>
				</div>

				<!--// Multiple entries -->
				<div class="row">
					<div class="col-sm-8">
						<h2>Senast uppladdade Petfies</h2>
					</div>

					<div class="col-sm-4">
						<div class="form-inline right">
							<div class="form-group">
								<input class="form-control" placeholder="Sök efter Petfie" id="search-petfie-2">
							</div>
							<button class="btn btn-success" id="search-petfie-submit-2">Sök</button>
						</div>
					</div>									
				</div>		

				<div class="row">
					<div class="col-sm-12 clearfix">
						<hr style="margin-top: 6px; margin-bottom: 6px;">
						<ul class="view-count">
							<li class="active">
								<a href="#">Visa 15 st</a>
							</li>
							<li>
								<a href="#">Visa 45 st</a>
							</li>
							<li>
								<a href="#">Visa alla</a>
							</li>															
						</ul>

						<h4 class="pull-right" style="margin-top: 0px;">
							Totalt antal <strong class="total-entries">0</strong>
						</h4>
					</div>
				</div>		

				<div class="row">
					<div class="col-sm-12">
						<hr style="margin-top: -4px; margin-bottom: 6px;">
					</div>
				</div>		


				<div class="row">
					<div class="col-sm-12">
						<p>Sortera efter: <button class="btn btn-default">Senast uppladdade</button></p>
					</div>
				</div>			

				<div class="row">
					<div class="col-sm-12">
						<div class="entry-list" id="entry-list-single"></div>
					</div>
				</div>								
			</div>

			<div class="disclaimer">
				<p class="text-muted">All persondata behandlas enligt PUL. Tävlingen är inte administrerad eller sponsrad av Facebook.</p>
			</div>		
		</div>
		<!-- #/Page: SINGLE -->		

	</div>	

	<!-- app init -->
	<script type="text/javascript">
		window.app = {
			appId: "<?php echo Settings::$_applications[$origin_id]['app_id']; ?>",
			locale: "<?php echo Settings::$_applications[$origin_id]['app_locale']; ?>",
			channel: "https://appclient.net/channel.php",
			accessToken: '',
			userId: '',
			signedRequest: '',
			namespace: '<?php echo Settings::$_applications[$origin_id]["app_namespace"]; ?>',
			pageId: "<?php echo Settings::$_applications[$origin_id]['page_id']; ?>",
			canvasURL: "https://apps.facebook.com/<?php echo Settings::$_applications[$origin_id]['app_namespace']; ?>",
			mobile: <?php echo ($mobile === true) ? "true" : "false"; ?>,
			directLink: "<?php echo $direct_link; ?>"
		}		
	</script>		
	
	<script src="js/jquery-2.0.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/countdown.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/load-image.min.js"></script>
	<script src="js/canvas-to-blob.min.js"></script>	
	<script src="js/jquery.ui.widget.js"></script>		
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="js/jquery.iframe-transport.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="js/jquery.fileupload.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="js/jquery.fileupload-process.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="js/jquery.fileupload-image.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="js/jquery.fileupload-audio.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="js/jquery.fileupload-video.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="js/jquery.fileupload-validate.js"></script>

	<script src="js/caman.full.min.js"></script>

	<script src="js/app.js"></script>
	
	<script type="text/javascript">
		window.fbAsyncInit = function() {
			FB.init({
					appId      : window.app.appId, 
					channelUrl : window.app.channel, 
					status	   : true,
					cookie     : false, 
					xfbml      : false 
			});
			
			FB.Canvas.setSize({height: 860});	
			
			$.ajaxSetup({
				url: 'ajax/brain.php',
				type: 'post',
				dataType: 'json', 
				data: {
					origin_id: window.app.appId					
				}
			}); 				
			
			FB.getLoginStatus(function(response) {			
				if (response.status === 'connected') {
					window.app.accessToken = response.authResponse.accessToken,
					window.app.signedRequest = response.authResponse.signedRequest,
					window.app.userId = response.authResponse.userID;
					
					// setup AJAX
					$.ajaxSetup({
						url: 'ajax/brain.php',
						type: 'post',
						dataType: 'json',
						data: {
							signed_request: window.app.signedRequest,
							access_token: window.app.accessToken,
							namespace: window.app.namespace,
							user_id: window.app.userId, 
							liked: <?php echo $liked == true ? "true" : "false"; ?>, 
							locale: window.app.locale, 
							origin_id: window.app.appId
						}
					});				
					
					// switch
					window.app.main.connected = true; 	
				}		
				
				/* =================================
					init application & load view
				================================= */		
				app.main.init();
			});			
		}
		
		$('body').append('<div id="fb-root"></div>');
		$.getScript(document.location.protocol + '//connect.facebook.net/' + window.app.locale + '/all.js');		
	</script>
</body>
</html>
