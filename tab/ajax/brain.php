<?php
/* ============================================================================
 * Copyright Facetime Media AB 2013
 * People:
 * @Kasian Marszalek (facebook.com/cassiusex)
============================================================================= */
ini_set('allow_url_fopen', 'on'); 

require_once('../../inc/lib/facebook.php');
require_once('../../lib/settings.php');
require_once('../../lib/instagram.class.php');
require_once('../../lib/UploadHandler.php');
require_once('../../lib/Carbon.php');
define(TEMPLATE_PATH, '../../template/'); 
define(UPLOAD_PATH, '../upload/'); 

use Carbon\Carbon;

// load vars
$db = new Database(); 
$required = array("action", "origin_id");
$optional = array("access_token", "namespace", "user_id", "long_url");

foreach ($required as $v) {
	if ( !isset($_POST[$v]) || !$_POST[$v] ) {
		$undefined = $v;
		exit("No sneaky without $v");
	}
	else {
		$$v = $db->escape($_POST[$v], false);
	}
}
foreach ($optional as $v) {
	$$v = isset($_POST[$v]) ? $_POST[$v] : '';
}


/* =================================
	AJAX MODEL
================================= */
class AjaxModel {
	private $db; 

	public function __construct() {	
		$this->db = new Database();  
	}	
	public function __destruct() {}


	/* =================================
		BASIC VIEW FUNCTION
	================================= */	
	public function returnView($which) {
		$return_array = array(
			'status' => '', 
			'html' => ''
		); 		
		
		
		$template = $this->returnTemplate($which); 
		if($template && $template != null) {
			$return_array['status'] = '200'; 
			$return_array['html'] = $template; 
		} else {
			$return_array['status'] = '404'; 
			$return_array['html'] = 'file not found';
		}
		
		return json_encode($return_array); 
	}

    public function uploadImage($user_id) {
        $required = array("user_name");

        foreach ($required as $v) {
            if ( !isset($_POST[$v]) || !$_POST[$v] ) {
                $undefined = $v;
                exit("No sneaky without $v");
            }
            else {
                $$v = $this->db->escape($_POST[$v], false);
            }
        }

        $settings = array(          
            'image_versions' => array(
                '' => array(
                    // Automatically rotate images based on EXIF meta data:
                    'auto_orient' => true, 
                    'max_width' => 800, 
                    'max_height' => 600
                ),
                'medium' => array(
                    'max_width' => 600,
                    'max_height' => 600, 
                    'crop' => true
                ),
                'thumbnail' => array(
                    // Uncomment the following to use a defined directory for the thumbnails
                    // instead of a subdirectory based on the version identifier.
                    // Make sure that this directory doesn't allow execution of files if you
                    // don't pose any restrictions on the type of uploaded files, e.g. by
                    // copying the .htaccess file from the files directory for Apache:
                    //'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')).'/thumb/',
                    //'upload_url' => $this->get_full_url().'/thumb/',
                    // Uncomment the following to force the max
                    // dimensions and e.g. create square thumbnails:
                    'crop' => true,
                    'max_width' => 200,
                    'max_height' => 300
                )
            )
        ); 
    
        // perform upload
        $upload = new UploadHandler($settings, false); 
        $response = $upload->post(false); 
        
        
        // get image object
        $image = $response["files"][0]; 
        
        // set vars for sql
        $user_id = $user_id; 
        $medium_url = $image->mediumUrl;        
        $thumb_url = $image->thumbnailUrl; 
        
        // perform SQL
        // $sql = "INSERT INTO zoozoo_petfie SET
        //         user_id = $user_id,
        //         user_name = '$user_name',
        //         medium_url = '$medium_url', 
        //         thumb_url = '$thumb_url', 
        //         insert_dt = NOW()"; 
                                    
        // $result = $this->db->query($sql); 
        // if($result && $result['result']) {
        //     return array(
        //         'status' => 'ok', 
        //         'image_medium' => $medium_url
        //     ); 
        // }

        if($medium_url != null) {
            return array(
                'status' => 'ok', 
                'image_thumb' => $thumb_url,
                'image_medium' => $medium_url
            );             
        }

        return array(
            'status' => 'fail'
        );         
    }    

    public function saveForm($user_id) {
        $required = array("user_name", "user_email", "pet_name", "pet_description");
        $optional = array("user_opt_email");

        foreach ($optional as $v) {
            $$v = isset($_POST[$v]) ? $_POST[$v] : '';
        }

        $undefined = array(); 
        foreach ($required as $v) {
            if ( !isset($_POST[$v]) || !$_POST[$v] ) {
                array_push($undefined, $v); 
            }
            else {
                $$v = $this->db->escape($_POST[$v], false);
            }
        }

        if(count($undefined) > 0) {
            return $undefined; 
        }

        $sql = "INSERT INTO zoozoo_petfie SET
                user_id = $user_id,
                user_name = '$user_name',
                user_email = '$user_email',
                pet_name = '$pet_name',
                pet_description = '$pet_description',
                medium_url = '', 
                thumb_url = '', 
                user_opt_email = '$user_opt_email'
                ON DUPLICATE KEY UPDATE
                user_name = '$user_name', 
                user_email = '$user_email', 
                pet_name = '$pet_name', 
                pet_description = '$pet_description', 
                user_opt_email = '$user_opt_email'";

                                    
        $result = $this->db->query($sql); 
        if($result && $result['result']) {
            return array(
                'status' => 'success' 
            );   
        }        

        return array(
            'status' => 'fail'
        );         
    }

    /* =================================
        INSTAGRAM MÖG
    ================================= */    
    public function getInstagramGallery() {
        $required = array("insta_username");

        foreach ($required as $v) {
            if ( !isset($_POST[$v]) || !$_POST[$v] ) {
                $undefined = $v;
                exit("No sneaky without $v");
            }
            else {
                $$v = $this->db->escape($_POST[$v], false);
            }
        }

        $instagram = new Instagram(array(
            'apiKey'      => 'a4d8ae23c62f40b7bb623ba1b1493c5e',
            'apiSecret'   => '66ae4d70a45142debc96d01e1b0dd530',
            'apiCallback' => 'http://appclient.net'
        ));

        $instagram->setAccessToken('2fa528e54b3245e696d8cc8d93dceef3');
        $users = $instagram->searchUser($insta_username, 1); 

        if($users->meta->code == 200) {
            $url = "https://api.instagram.com/v1/users/" . $users->data[0]->id . "/media/recent/?client_id=a4d8ae23c62f40b7bb623ba1b1493c5e&limit=100"; 
            $gallery = json_decode(file_get_contents($url)); 

            if($gallery->meta->code == 200) {
                if(count($gallery->data) < 1) {
                    return array(
                        'error' => true, 
                        'why' => 'Det finns inga uppladdade bilder under profilen.'
                    ); 
                }

                // ALLT OK
                return $gallery; 
            } else if($gallery->meta->code == 400) {
                return array(
                    'error' => true, 
                    'why' => 'Profilen finns inte eller är den privat.'
                ); 
            }
        }
    }       
    
    
    /* =================================
        INSTAGRAM MÖG
    ================================= */    
    public function getGallery() {
            
        /* =================================
            MERGE MED DATABAS
        ================================= */            
        $sql = "SELECT * FROM zoozoo_petfie
                ORDER BY insert_dt DESC"; 
                                    
        $mysql_result = $this->db->query($sql); 
        if($mysql_result && $mysql_result['num_rows'] > 0) {
            /* =================================
                MERGE DEM ARRAYS
            ================================= */        
            $mysql_result = $mysql_result['result']; 
        } 
        
        if(isset($mysql_result['num_rows']) && $mysql_result['num_rows'] < 1) {
            return array(
                'data' => 'no_entries', 
            ); 
        }       

        $result = array(
            'data' => $mysql_result, 
        ); 

        return $result;         
    }   

    /* =================================
        INSTAGRAM MÖG
    ================================= */    
    public function getGallerySearch() {
        $required = array("query");

        foreach ($required as $v) {
            if ( !isset($_POST[$v]) || !$_POST[$v] ) {
                $undefined = $v;
                exit("No sneaky without $v");
            }
            else {
                $$v = $this->db->escape($_POST[$v], false);
            }
        }

        /* =================================
            MERGE MED DATABAS
        ================================= */            
        $sql = "SELECT * FROM zoozoo_petfie
                WHERE user_name LIKE '%$query%'
                ORDER BY insert_dt DESC"; 
                                    
        $mysql_result = $this->db->query($sql); 
        if($mysql_result && $mysql_result['num_rows'] > 0) {
            /* =================================
                MERGE DEM ARRAYS
            ================================= */        
            $mysql_result = $mysql_result['result']; 
        } 
        
        if(isset($mysql_result['num_rows']) && $mysql_result['num_rows'] < 1) {
            return array(
                'data' => 'no_entries', 
            ); 
        }       

        $result = array(
            'data' => $mysql_result, 
        ); 

        return $result;         
    }       

    
    public function uploadImageFacebook($user_id) {
        $required = array("user_name", "image_large", "filter");

        foreach ($required as $v) {
            if ( !isset($_POST[$v]) || !$_POST[$v] ) {
                $undefined = $v;
                exit("No sneaky without $v");
            }
            else {
                $$v = $this->db->escape($_POST[$v], false);
            }
        }

        $image_large = str_replace('data:image/png;base64,', '', $image_large);
        $image_large = str_replace(' ', '+', $image_large);
        $data = base64_decode($image_large);
        $img_name = $user_id . uniqid() . md5(microtime()) . '.png'; 
        $image = UPLOAD_PATH . $img_name;

        if(file_put_contents($image, $data)) {
            $image = "https://appclient.net/custom_htdocs/zoozoo/tab/upload/" . $img_name; 

            // set vars for sql
            $user_id = $user_id; 
            $medium_url = $image;         
            $thumb_url = $image; 
            $user_filter = $filter; 
            
            // perform SQL
            $sql = "INSERT INTO zoozoo_petfie SET
                    user_id = $user_id,
                    user_name = '$user_name',
                    medium_url = '$medium_url', 
                    thumb_url = '$thumb_url', 
                    filter = '$user_filter',
                    insert_dt = NOW() 
                    ON DUPLICATE KEY UPDATE
                    user_name = '$user_name',
                    medium_url = '$medium_url', 
                    thumb_url = '$thumb_url',
                    filter = '$user_filter'"; 
                                        
            $result = $this->db->query($sql); 
            if($result && $result['result']) {
                return array(
                    'status' => 'success', 
                    'error' => false
                ); 
            }
        }

        return array(
            'status' => 'failure', 
            'error' => true
        );      
    }    

    
    public function shortenUrl($url) {
        $curlObj = curl_init();
         
        $url_data = array('longUrl' => $url);
        $jsonData = json_encode($url_data);
         
        curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url');
        curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlObj, CURLOPT_HEADER, 0);
        curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
        curl_setopt($curlObj, CURLOPT_POST, 1);
        curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
         
        $response = curl_exec($curlObj);
         
        //change the response json string to object
        $google_response = json_decode($response);
        curl_close($curlObj);
         
        return array(
            'short_url' => $google_response->id 
        );            
    }


    public function entry_exists($check_user_id) {
        $sql = "SELECT * FROM zoozoo_petfie WHERE user_id = $check_user_id"; 
        $result = $this->db->query($sql);

        if ($result && $result['num_rows'] > 0) {           
            return true; 
        }       
        
        return false;  
    }      

	/* =================================
		PRIVATE FUNCTIONS
	================================= */	
	private function returnTemplate($which) {
		if (!$which || $which === '')
			throw new AppException ( 'Template file was not specified ' );
		try	{
			$template =  $this->get_include_contents(TEMPLATE_PATH . "/$which.php");
		} catch (AppException $e) {
			AppException::handleError($e);
		}
		
		return $template;
	}		

	private function get_include_contents($filename) {
	    if (is_readable($filename)) {
	        ob_start();
	        include $filename;
	        return ob_get_clean();
	    }
	    
	    return false;
	}		

    private function can_vote($user_id, $entry_id) {        
        $this->entry_exists($entry_id); 
    
        $sql = "SELECT * FROM zoozoo_petfie_likes WHERE liker_id = $user_id AND entry_id = $entry_id ORDER BY insert_dt DESC";
        $result = $this->db->query($sql);

        if ($result && $result['num_rows'] > 0) {               
            return false; 
        }       
    
        return true; 
    }

    // Vote (add likes)
    public function like() {
        $required = array('user_id', 'entry_id'); 

        foreach ($required as $v) {
            if ( !isset($_POST[$v]) || $_POST[$v] == '') {
                return "missing_field:$v";
            }
            else {
                $$v = $this->db->escape($_POST[$v]);
            }
        }


        // If cannot vote, return error
        if(!$this->can_vote($user_id, $entry_id)) {
            return array('error' => 'already_voted'); 
        }

        // perform SQL
        $sql = "INSERT INTO zoozoo_petfie_likes SET
                entry_id = $entry_id,
                liker_id = $user_id,
                insert_dt = NOW()"; 
                                    
        $result = $this->db->query($sql); 
        if($result && $result['result']) {
            return array(
                'status' => 'success', 
                'error' => false
            ); 
        }

        return array(
            'status' => 'failure', 
            'error' => false
        );         
    }       
    

    // Get single entry with entry_id
    public function get_single_entry() {
        $required = array('user_id'); 

        foreach ($required as $v) {
            if ( !isset($_POST[$v]) || $_POST[$v] == '') {
                return "missing_field:$v";
            }
            else {
                $$v = $this->db->escape($_POST[$v]);    
            }
        }
                
        $sql = "SELECT * FROM zoozoo_petfie
                WHERE zoozoo_petfie.user_id = $user_id"; 
    
        $result = $this->db->query($sql);

        if ($result && $result['num_rows'] > 0) {     
            $sql_likes = "SELECT count(*) as num_likes FROM zoozoo_petfie_likes
            WHERE zoozoo_petfie_likes.entry_id = $user_id"; 
            $result_likes = $this->db->query($sql_likes);

            $sql_comments = "SELECT * FROM zoozoo_petfie_comments
            WHERE zoozoo_petfie_comments.entry_id = $user_id
            ORDER BY insert_dt DESC"; 
            $result_comments = $this->db->query($sql_comments);            

            return array(
                'exists' => true,
                'entry' => $result['result'][0],
                'likes' => $result_likes['result'][0]['num_likes'],
                'comments' => $result_comments['result'], 
                'short_url' => $this->shortenUrl('appclient.net/custom_htdocs/zoozoo/tab/share.php?origin_id=1411414342426920&id=' . $user_id)['short_url']
            ); 
        }       
        
        return array('error' => 'no_result'); 
    }     


    public function get_entries() {
        $optional = array("search_term");

        foreach ($optional as $v) {
            $$v = isset($_POST[$v]) ? $this->db->escape($_POST[$v]) : null;
        }
                
        $sql = "SELECT * FROM zoozoo_petfie WHERE pet_name != '' AND pet_description != ''"; // Pagination
        if(isset($search_term) && strlen($search_term) > 0) {
            $sql = "SELECT * FROM zoozoo_petfie WHERE user_name LIKE '%$search_term%' OR pet_name LIKE '%$search_term%' AND pet_name != '' AND pet_description != ''"; // Pagination
        }
    
        $result = $this->db->query($sql);
        $all_results = array(); 

        if ($result && $result['num_rows'] > 0) {     
            foreach($result['result'] as $entry) {
                $user_id = $entry['user_id']; 

                $sql_likes = "SELECT count(*) as num_likes FROM zoozoo_petfie_likes
                WHERE zoozoo_petfie_likes.entry_id = $user_id"; 
                $result_likes = $this->db->query($sql_likes);

                $sql_comments = "SELECT * FROM zoozoo_petfie_comments
                WHERE zoozoo_petfie_comments.entry_id = $user_id
                ORDER BY insert_dt DESC"; 
                $result_comments = $this->db->query($sql_comments);                  


                $built_entry = array(
                    'exists' => true,
                    'entry' => $entry, 
                    'likes' => $result_likes['result'][0]['num_likes'],
                    'comments' => $result_comments['result'], 
                ); 

                array_push($all_results, $built_entry); 
            }       

            return $all_results; 
        }       
        
        return array('error' => 'no_result');         
    }

    public function get_entries_top() {                
        $sql = "SELECT zoozoo_petfie.*, count(zoozoo_petfie_likes.entry_id) as like_count FROM zoozoo_petfie LEFT JOIN zoozoo_petfie_likes ON zoozoo_petfie.user_id = zoozoo_petfie_likes.entry_id WHERE zoozoo_petfie.pet_name <> '' AND zoozoo_petfie.pet_description <> '' GROUP BY zoozoo_petfie.user_id ORDER BY like_count DESC LIMIT 3"; // Pagination
    
        $result = $this->db->query($sql);
        $all_results = array(); 

        if ($result && $result['num_rows'] > 0) {     
            foreach($result['result'] as $entry) {
                $user_id = $entry['user_id']; 

                $sql_likes = "SELECT count(*) as num_likes FROM zoozoo_petfie_likes
                WHERE zoozoo_petfie_likes.entry_id = $user_id"; 
                $result_likes = $this->db->query($sql_likes);

                $sql_comments = "SELECT * FROM zoozoo_petfie_comments
                WHERE zoozoo_petfie_comments.entry_id = $user_id
                ORDER BY insert_dt DESC"; 
                $result_comments = $this->db->query($sql_comments);                  


                $built_entry = array(
                    'exists' => true,
                    'entry' => $entry, 
                    'likes' => $result_likes['result'][0]['num_likes'],
                    'comments' => $result_comments['result'], 
                ); 

                array_push($all_results, $built_entry); 
            }       

            return $all_results; 
        }       
        
        return array('error' => 'no_result');         
    }

    // Add new comment 
    public function add_comment() {
        $required = array('user_id', 'entry_id', 'user_name', 'comment_text'); 

        foreach ($required as $v) {
            if ( !isset($_POST[$v]) || $_POST[$v] == '') {
                return "missing_field:$v";
            }
            else {
                $$v = $this->db->escape($_POST[$v]);
            }
        }

        /** 

            THROTTLE

        **/ 

        // perform SQL
        $sql = "INSERT INTO zoozoo_petfie_comments SET
                entry_id = $entry_id,
                user_id = $user_id,
                user_name = '$user_name',
                comment_text = '$comment_text', 
                insert_dt = NOW()"; 
                                    
        $result = $this->db->query($sql); 
        if($result && $result['result']) {
            return array(
                'status' => 'success', 
                'error' => false
            ); 
        }

        return array(
            'status' => 'failure', 
            'error' => false
        );  
    }    

    public function save_insta() {
        $required = array('user_id', 'image_medium', 'image_thumb'); 

        foreach ($required as $v) {
            if ( !isset($_POST[$v]) || $_POST[$v] == '') {
                return "missing_field:$v";
            }
            else {
                $$v = $this->db->escape($_POST[$v]);
            }
        }

        $image_medium = stripcslashes($image_medium); 
        $image_thumb = stripcslashes($image_thumb);

        $image_type = explode(".", $image_medium);
        $image_type = end($image_type); 

        $img_name = $user_id . md5(microtime()) . "." . $image_type; 
        $img_name_thumb = "thumb-" . $user_id . md5(microtime()) . "." . $image_type; 

        $img = UPLOAD_PATH . $img_name; 
        $img_thumb = UPLOAD_PATH . $img_name_thumb; 

        if(file_put_contents($img, file_get_contents($image_medium))) {
            if(file_put_contents($img_thumb, file_get_contents($image_thumb))) {
                return array(
                    'status' => 'ok', 
                    'image_medium' => 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/' . $img_name,
                    'image_thumb' => 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/' . $img_name_thumb
                );             
            }
        }  

        return array(
            'status' => 'fail',
            'error' => false
        );         
    }      

    public function delete_entry() {
        $required = array('user_id'); 

        foreach ($required as $v) {
            if ( !isset($_POST[$v]) || $_POST[$v] == '') {
                return "missing_field:$v";
            }
            else {
                $$v = $this->db->escape($_POST[$v]);
            }
        }
        $sql = "DELETE FROM zoozoo_petfie WHERE user_id = $user_id"; 

        $sql2 = "DELETE FROM zoozoo_petfie_likes WHERE entry_id = $user_id"; 

        $sql3 = "DELETE FROM zoozoo_petfie_comments WHERE entry_id = $user_id";                 

        $result = $this->db->query($sql); 
        if($result && $result['result']) {
            $result2 = $this->db->query($sql2); 
            if($result2 && $result2['result']) {
                $result3 = $this->db->query($sql3); 
                if($result3 && $result3['result']) {

                    return array(
                        'status' => 'deleted', 
                        'error' => false
                    ); 
                }
            }
        }

        return array(
            'status' => 'failure', 
            'error' => true, 
            'result' => $result2
        );                 
    } 
}


$model = new AjaxModel();

/* =================================
	Action controller (typ)
================================= */
switch ($action) {	
	case 'initview' : 
		echo $model->returnView('start'); 
	break;   

    case 'shorten_url' : 
        echo json_encode($model->shortenUrl($long_url)); 
    break; 

    case 'signup' :
        $result = $model->saveForm($user_id); 
        echo json_encode($result); 
    break;     

    /* =================================
        INSTAGRAM GALLERY
    ================================= */        
    case 'getGallery' : 
        $result = $model->getGallery(); 
        echo json_encode($result); 
    break; 

    case 'getGallerySearch' : 
        $result = $model->getGallerySearch(); 
        echo json_encode($result); 
    break;  

    case 'getInstagramGallery' : 
        $result = $model->getInstagramGallery(); 
        echo json_encode($result);  
    break; 
    
    case 'uploadImage' :        
        $result = $model->uploadImage($user_id); 
        echo json_encode($result); 
    break; 

    case 'uploadFacebookImage' : 
        $result = $model->uploadImageFacebook($user_id); 
        echo json_encode($result); 
    break;  
    
    case 'get_entry' : 
        $result = $model->get_single_entry($user_id); 
        echo json_encode($result); 
    break;     

    case 'get_entries' : 
        $result = $model->get_entries(); 
        echo json_encode($result);     
    break; 

    case 'get_entries_top' : 
        $result = $model->get_entries_top(); 
        echo json_encode($result);     
    break;     

    case 'save_insta' : 
        $result = $model->save_insta(); 
        echo json_encode($result); 
    break;

    case 'like' : 
        $result = $model->like(); 
        echo json_encode($result); 
    break;    

    case 'post_comment' : 
        $result = $model->add_comment(); 
        echo json_encode($result);     
    break;  

    case 'delete_entry' : 
        $result = $model->delete_entry(); 
        echo json_encode($result); 
    break; 

	default:
		$return_array = array(
			'status' => '500', 
			'html' => 'invalid operation request'
		); 
		
		echo json_encode($return_array); 
	break; 
}
?>