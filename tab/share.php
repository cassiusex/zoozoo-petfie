<?php 
require_once('../lib/settings.php');
require_once('../inc/lib/facebook.php'); 


/* ============================================================================
	APP INIT
============================================================================= */
$db = new Database(); 
$origin_id = $db->escape($_GET['origin_id']); 
$petfie_id = $db->escape($_GET['id']); 


if(isset($_COOKIE["ftm_petfie_id"]) && $_COOKIE["ftm_petfie_id"] != null) {
	setcookie ("ftm_petfie_id", "", time() - 3600);
}


setcookie("ftm_petfie_id", $petfie_id); 
// Exit
?>

<html>
    <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# zoozoo_petfie: http://ogp.me/ns/fb/zoozoo_petfie#">
        <title>Zoozoo Petfie!</title>
        
        <meta property="og:title" content="Zoozoo Petfie!" />
        <meta property="og:image" content="https://appclient.net/custom_htdocs/zoozoo/tab/img/400-share.png" />
        <meta property="og:description" content="Rösta på mitt bidrag i årets roligaste tävling för djurägare!" />
        <meta property="fb:app_id" content="1411414342426920" />
        <meta property="og:url" content="https://appclient.net/custom_htdocs/zoozoo/tab/share.php?origin_id=1411414342426920&id=<?php echo $petfie_id; ?>" />
    </head>
    <body></body>
</html>

<script>
    window.location.replace("http://apps.facebook.com/zoozoo_petfie");
</script>