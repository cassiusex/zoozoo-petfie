-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Värd: localhost
-- Skapad: 14 okt 2014 kl 18:36
-- Serverversion: 5.5.28-cll
-- PHP-version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databas: `aclient_customprojects`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `zoozoo_petfie_likes`
--

CREATE TABLE IF NOT EXISTS `zoozoo_petfie_likes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `entry_id` bigint(20) unsigned NOT NULL,
  `liker_id` bigint(20) unsigned NOT NULL,
  `insert_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumpning av Data i tabell `zoozoo_petfie_likes`
--

INSERT INTO `zoozoo_petfie_likes` (`id`, `entry_id`, `liker_id`, `insert_dt`) VALUES
(29, 868005144, 579085030, '2014-09-11 15:07:34'),
(27, 597884551, 579085030, '2014-09-11 14:40:00'),
(36, 579085030, 579085030, '2014-09-11 16:01:04'),
(40, 624464754, 624464754, '2014-09-12 13:56:49'),
(39, 785090143, 785090143, '2014-09-12 13:41:11'),
(38, 835060267, 624464754, '2014-09-12 13:34:26'),
(34, 597884551, 579085030, '2014-09-11 15:13:55'),
(41, 785090143, 840925719, '2014-09-15 09:42:43'),
(42, 597884551, 840925719, '2014-09-15 09:43:51');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
