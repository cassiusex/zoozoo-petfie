-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Värd: localhost
-- Skapad: 14 okt 2014 kl 18:36
-- Serverversion: 5.5.28-cll
-- PHP-version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databas: `aclient_customprojects`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `zoozoo_petfie_comments`
--

CREATE TABLE IF NOT EXISTS `zoozoo_petfie_comments` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `entry_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `comment_text` varchar(400) NOT NULL,
  `insert_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumpning av Data i tabell `zoozoo_petfie_comments`
--

INSERT INTO `zoozoo_petfie_comments` (`id`, `entry_id`, `user_id`, `user_name`, `comment_text`, `insert_dt`) VALUES
(45, 785090143, 840925719, 'Jenny Bergendahl', 'Sötis-sulan <3', '2014-09-15 09:43:38'),
(42, 579085030, 624464754, 'Adam Lund Correa', 'fin katt', '2014-09-11 17:05:43'),
(37, 597884551, 579085030, 'Kasian Marszalek', 'hej igen!', '2014-09-11 15:13:20'),
(38, 597884551, 579085030, 'Kasian Marszalek', 'hjsadasd', '2014-09-11 15:13:38'),
(43, 579085030, 835060267, 'Viktor Göransson', 'Nej @Adam Lund Correa', '2014-09-12 08:54:54'),
(44, 835060267, 624464754, 'Adam Lund Correa', 'Hey', '2014-09-12 13:56:34'),
(35, 868005144, 579085030, 'Kasian Marszalek', 'Hej hej', '2014-09-11 15:12:12'),
(34, 597884551, 579085030, 'Kasian Marszalek', 'hej hej!', '2014-09-11 14:47:33');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
