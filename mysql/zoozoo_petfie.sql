-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Värd: localhost
-- Skapad: 14 okt 2014 kl 18:36
-- Serverversion: 5.5.28-cll
-- PHP-version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databas: `aclient_customprojects`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `zoozoo_petfie`
--

CREATE TABLE IF NOT EXISTS `zoozoo_petfie` (
  `user_id` bigint(20) unsigned NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `pet_name` varchar(255) NOT NULL,
  `pet_description` text NOT NULL,
  `medium_url` text NOT NULL,
  `thumb_url` text NOT NULL,
  `filter` varchar(255) NOT NULL,
  `user_opt_email` varchar(6) NOT NULL,
  `insert_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumpning av Data i tabell `zoozoo_petfie`
--

INSERT INTO `zoozoo_petfie` (`user_id`, `user_name`, `user_email`, `pet_name`, `pet_description`, `medium_url`, `thumb_url`, `filter`, `user_opt_email`, `insert_dt`) VALUES
(785090143, 'Hanna Moisander', 'Hanna@icgab.com', 'ZooZoo-Sulan', '#tyckersyndommigsjälvpetfie', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/7850901435412f702b3e512db30d930e06f089591c547945629b7a.png', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/7850901435412f702b3e512db30d930e06f089591c547945629b7a.png', 'clear', 'on', '2014-09-12 13:37:06'),
(868005144, 'Fredrik Svensson', 'katt', 'Katt', 'Mjau', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/5790850305411b6a9ae67083523a2a29e201ff43d878476b709781.png', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/5790850305411b6a9ae67083523a2a29e201ff43d878476b709781.png', 'oldBoot', 'on', '2014-09-11 14:50:17'),
(597884551, 'Mattias Marmberg', 'hej@hej.se', 'Liten katt', 'hej hej', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/5790850305411acdf3f7e82b322fdee7b00022a857bd087b687051.png', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/5790850305411acdf3f7e82b322fdee7b00022a857bd087b687051.png', 'oldBoot', 'on', '2014-09-11 14:08:31'),
(835060267, 'Viktor Göransson', 'as@as.com', 'AS', 'aipdns', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/8350602675412af8086b68db4921be4ce0c0c8f8928c3cedfa277a.png', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/8350602675412af8086b68db4921be4ce0c0c8f8928c3cedfa277a.png', 'lomo', 'on', '2014-09-11 14:48:09'),
(579085030, 'Kasian Marszalek', 'katt@katt.se', 'Kyjse myjs', 'Kyyyyjse myyyyyyyyjs', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/5790850305411c72df2c326d70e9884bf141a604f9c0c25ae8e5b9.png', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/5790850305411c72df2c326d70e9884bf141a604f9c0c25ae8e5b9.png', 'oldBoot', 'on', '2014-09-11 16:00:45'),
(624464754, 'Adam Lund Correa', 'adam@zoozoo.com', 'adam', 'Levande beskrivning', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/6244647545412f56b6320239c019187e61e5334ee820f6798b8834.png', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/6244647545412f56b6320239c019187e61e5334ee820f6798b8834.png', 'oldBoot', 'on', '2014-09-12 13:30:19'),
(840925719, 'Jenny Bergendahl', 'bergendahl@outlook.com', 'Filippa K', 'FK', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/8409257195416b3a220451bd36aa8c82586822a94dc3ce11252365.png', 'https://appclient.net/custom_htdocs/zoozoo/tab/upload/8409257195416b3a220451bd36aa8c82586822a94dc3ce11252365.png', 'clear', '', '2014-09-15 09:38:42');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
