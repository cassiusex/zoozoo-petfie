<?php
/** ===============
	Canvas app redirect
	@Kasian Marszalek, 2013
	
	
	Add page to tab: 
	https://www.facebook.com/dialog/pagetab?app_id=474468195995371&redirect_uri=http://facebook.com
================ */

require_once('lib/settings.php'); 
require_once('inc/lib/facebook.php');
	
$db = new Database(); 	
$origin_id = $db->escape($_GET['origin_id']); 	
$facebook = new Facebook(array(
	'appId' => Settings::$_applications[$origin_id]['app_id'],
	'secret' => Settings::$_applications[$origin_id]['app_secret'],
	'cookie' => true,
	'xfbml' => true,
	'oauth' => true
));

$user = $facebook->getUser();
if (!$user) {
	$loginUrl = $facebook->getLoginUrl(array('scope' => ''));
	echo("<script>top.location.href='" . $loginUrl . "'</script>");
};
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo Settings::$_applications[$origin_id]['app_name']; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="cache-control" content="no-cache">
	<meta property="og:title" content="Roadtrip-testet!"/>
	<meta property="og:url" content="http://apps.facebook.com/bmw_roadtrip"/>
	<meta property="og:image" content="http://appclient.net/custom_htdocs/bmw_roadtrip/tab/img/400-share.png"/>
	<meta property="og:description" content="Kanal 5 och BMW Financial Services presenterar Roadtrip-testet!  Planerar du en roadtrip i sommar? Vi hjälper dig att hitta din mest lämpliga bilkompis och du får även chansen att tävla om en BMW till din roadtrip." />

	<!-- css includes -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animate.css">	
	<link rel="stylesheet" href="css/style.css">	
</head>
<body>
</body>
</html>


<?php
$tab_url = 'https://www.facebook.com/' . Settings::$_applications[$origin_id]['page_id'] . '?sk=app_' . Settings::$_applications[$origin_id]['app_id']; 
echo "<script>top.location.href='" . $tab_url . "'</script>";
?>